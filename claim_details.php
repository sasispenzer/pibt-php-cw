<?php
/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */
error_reporting(0);
include_once("common_header.php");
// header section include
include_once('layouts/header.php');

//side Navigation include
include_once('layouts/side_navigation.php');
?>
<?php
// Spenzer Starts Here
$claim_obj = new Claim();
$claim_id = $_GET['claim_id'];
$minutes_obj = new Minutes();
$minutes_data =  $minutes_obj->getMinutesByClaimID($claim_id);

if($claim_id == '' || $claim_id == 0){
    header("Location:view_claims.php");
}else{

    $claim_details = $claim_obj->getClaimsByID($claim_id);
    $subject_id = $claim_details['subject_id'];
    if(!empty($subject_id)){
        $subject_obj = new Subject();
        $subject_data = $subject_obj->getSubjectNameByID($subject_id);
    }
    // Get Claim Types
    $claimType_obj = new ClaimType();
    $ClaimTypes = $claimType_obj->getAllClaimTypes();

    $current_user = $claim_details['student_id'];
    $current_user_program = $claim_details['program_id'];
    // getProgram from DB
    $program_obj = new Program();
    $program_data = $program_obj->getProgramById($current_user_program);

    $subject_obj = new Subject();
    $subjectIDArray = $subject_obj->getSubjectsByPid($current_user_program);
    $user_subjects_array = array();
    foreach($subjectIDArray as $eachID){
        $sub_details = $subject_obj->getSubjectNameByID($eachID['sub_id']);
        $array = array();
        $array['subject_name'] = $sub_details['subject_name'];
        $array['id'] = $sub_details['id'];
        array_push($user_subjects_array,$array);
    }
}

// when Submitting

if(isset($_POST['submit_stu'])){

    if(!empty($_POST)){
        $claim_data = array();
        $claim_data['claim_title'] =  isset($_POST ['claim_title']) ? $_POST ['claim_title'] : '';
        $claim_data['claim_type_id'] =  isset($_POST ['claim_type']) ? $_POST ['claim_type'] : '';
        $claim_data['claim_description'] =  isset($_POST ['claim_description']) ? $_POST ['claim_description'] : '';

        $claim_id =  isset($_POST ['claim_id_hidden']) ? $_POST ['claim_id_hidden'] : '';

        try {

            if ($claim_data['claim_title'] == '') throw new Exception("Please Enter a Valid Claim Title");
            if ($claim_data['claim_type_id'] == '') throw new Exception("Please Enter a Valid Claim Type");
            if ($claim_data['claim_description'] == '') throw new Exception("Please Enter a Valid Claim Description");

            $claim_obj->updateClaim($claim_data,$claim_id);
            if(count($_FILES['evidence'])) {
                $count = 0;
                define("UPLOAD_DIR", "uploads/evidence/");
                foreach ($_FILES['evidence']['name'] as $file) {

                    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $file);
                    $i = 0;
                    $parts = pathinfo($file);
                    $extentionArray = array("jpg","JPG","JPEG","jpeg","png","PNG","PDF","pdf");
                    if (!in_array($parts["extension"], $extentionArray)) {
                        throw new Exception("Your Evidence file must be in PDF/PNG or JPG Formats !");
                    }
                    while(file_exists(UPLOAD_DIR . $name)) {
                        $i++;
                        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
                    }
                    $data_array = array();
                    $data_array['evidence_image'] = $name;
                    $data_array['claim_id'] = $claim_id;
                    $claim_obj->saveEvidece($data_array);
                    $success = move_uploaded_file($_FILES['evidence']["tmp_name"][$count], UPLOAD_DIR . $name);
                    // set proper permissions on the new file
                    chmod(UPLOAD_DIR . $name, 0644);
                    $count++;
                }
            }
            $success = "Your Claim Updated Successfully !" ;

        }catch (Exception $exc) {
            $error_message = $exc->getMessage();
        }

    }
}
if(isset($_POST['submit_staff'])){
    $claim_data = array();
    $claim_data['status'] =  isset($_POST ['status']) ? $_POST ['status'] : '';

    $claim_id =  isset($_POST ['claim_id_hidden']) ? $_POST ['claim_id_hidden'] : '';
    try {

        if ($claim_data['status'] == '') throw new Exception("Please Enter a Valid Status");


        $claim_obj->updateClaim($claim_data,$claim_id);

        $success = "Your Claim Updated Successfully !" ;

    }catch (Exception $exc) {
        $error_message = $exc->getMessage();
    }
}

if(isset($_POST['add_minutes'])){
    $minutes_data = array();
    $minutes_data['claim_minute'] =  isset($_POST ['minutes']) ? $_POST ['minutes'] : '';
    $minutes_data['claim_id'] =   $claim_id;
    $minutes_data['user_id'] =   $_SESSION['user_id'];
    $minutes_data['date'] =   date('Y-m-d');
    try {
        if ($minutes_data['claim_minute'] == '') throw new Exception("Please Enter a Valid Minute !");
        $minutes_obj->add_minute($minutes_data);

        $success = "Your Claim Minute added Successfully !" ;

    }catch (Exception $exc) {
        $error_message = $exc->getMessage();
    }

}



?>




<!-- Main Content Starts -->


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Claim Management
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Claim details</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Claim details</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">

                            </div>
                            <?php  if(isset($error_message)) { ?>
                                <div class="alert-danger alert">
                                    <?php   echo $error_message; ?>
                                </div>
                            <?php } ?>
                            <?php  if(isset($success)) { ?>
                                <div class="alert alert-success">
                                    <?php echo $success; ?>
                                </div>
                            <?php } ?>

                                <!-- /.box-header -->
                                <!-- form start -->
                            <form enctype="multipart/form-data" method="post" role="form">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="title">Claim Title</label>
                                        <input <?php if($_SESSION['user_role'] != 1){?> readonly <?php } ?> type="text" class="form-control" id="claim_title" value="<?php echo $claim_details['claim_title'] ; ?>" name="claim_title">
                                        <input type="hidden" name="claim_id_hidden" value="<?php echo  $claim_details['id']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Claim Type</label>
                                        <select <?php if($_SESSION['user_role'] != 1){?> disabled <?php } ?> class="form-control" name="claim_type" id="claim_type">
                                            <option value="<?php echo $claim_details['claim_id']; ?>"><?php echo $claim_details['claim_type']; ?></option>
                                            <?php
                                            foreach($ClaimTypes as $eachType){?>

                                                <option id="<?php echo $eachType['id'] ; ?>" value="<?php echo $eachType['id'] ; ?>"><?php echo $eachType['claim_type'] ; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div id="fname" class="form-group">
                                        <label for="title">Faculty Name</label>
                                        <input value="<?php echo $claim_details['faculty_name'] ; ?>" type="text" readonly class="form-control" id="f_id" name="f_id">
                                    </div>
                                    <div id="pname" class="form-group">
                                        <label for="title">Program Name</label>
                                        <input value="<?php echo $program_data['program_name'] ; ?>" type="text" readonly class="form-control" id="p_id" name="p_id">
                                    </div>
                                    <?php if(isset($subject_data)){ ?>
                                    <div id="sname" style="display: none"  class="form-group">
                                        <label for="subjecta">Select a Subject</label>
                                        <select class="form-control" name="subject" id="subject">
                                            <option value="<?php echo $subject_data['id'] ; ?>"><?php echo $subject_data['subject_name']; ?></option>
                                            <?php foreach($user_subjects_array as $each_sub){ ?>
                                                <option value="<?php echo $each_sub['id'] ; ?>" id="<?php echo $each_sub['id'] ; ?>"><?php echo $each_sub['subject_name'] ; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label>Claim Description</label>
                                        <textarea <?php if($_SESSION['user_role'] != 1){?> readonly <?php } ?> id="claim_description"  name="claim_description" class="form-control" rows="3" ><?php echo $claim_details['claim_description'] ; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Upload More Evidence</label>
                                        <input type="file" name="evidence[]"  multiple="" id="evidence">
                                        <p class="help-block">File Format - PDF / JPG / PNG.</p>
                                        <ul id="filelist">

                                        </ul>
                                    </div>
                                    <?php if($_SESSION['user_role'] != 1){?>
                                        <div class="form-group">
                                            <label>Claim Current Status</label>
                                             <input value="0" <?php if($claim_details['status'] == 0){?> checked="checked"<?php } ?> type="radio" name="status" id="radio_pending"> <small class="label bg-black-gradient">Pending Approval</small>
                                             <input value="1" <?php if($claim_details['status'] == 1){?> checked="checked"<?php } ?>  type="radio" name="status" id="radio_nortified"> <small class="label  bg-yellow">Notified. Processing Claim</small>
                                             <input value="3" <?php if($claim_details['status'] == 3){?> checked="checked"<?php } ?>  type="radio" name="status" id="radio_taken"> <small class="label  bg-green">Action Taken</small>
                                             <input value="4" <?php if($claim_details['status'] == 4){?> checked="checked"<?php } ?>  type="radio" name="status" id="radio_rejected"> <small class="label  bg-red-gradient">Rejected</small>

                                        </div>


                                    <?php }  ?>
                                    <div>
                                        <label>Evidence File List</label>
                                        <ul>
                                            <?php
                                            $claim_evidence = $claim_obj->getEvidence($claim_details['id']);
                                            if(empty($claim_evidence)){?>
                                                <li>
                                                  No Evidence to this Claim !
                                                </li>
                                            <?php }else{
                                            foreach($claim_evidence as $each_one){?>
                                                    <a href="uploads/evidence/<?php echo $each_one['evidence_image']; ?>">
                                                        <li>
                                                            <?php echo $each_one['evidence_image']; ?>
                                                        </li>
                                                    </a>
                                            <?php }}
                                            ?>
                                        </ul>
                                    </div>
                                </div>

                                <!-- /.box-body -->
                                <?php if($_SESSION['user_role'] == 1){?>
                                    <div class="box-footer">
                                        <button name="submit_stu" type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                <?php } else{ ?>
                                    <div class="box-footer">
                                        <button name="submit_staff" type="submit" class="btn btn-primary">Update Claim</button>
                                    </div>
                                <?php }  ?>
                                <div class="box box-success">
                                    <div class="box-header">
                                        <i class="fa fa-comments-o"></i>

                                        <h3 class="box-title">Claim Minutes</h3>

                                        <div class="box-tools pull-right" data-toggle="tooltip" title="Status">

                                        </div>
                                    </div>
                                    <div class="box-body chat" id="chat-box">
                                        <!-- chat item -->
                                    <?php if(empty($minutes_data)) {?>
                                        <div class="item">


                                            <p style="margin-top: auto" class="message">

                                                No Minutes for this Claim yet !
                                            </p>
                                        </div>
                                    <?php } else {
                                    foreach($minutes_data as $each_one) { ?>
                                            <div class="item">
                                                <img src="dist/img/54-128.png" alt="user image" class="offline">
                                                <p class="message">
                                                    <a href="#" class="name">
                                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i><?php echo $each_one['date'] ; ?></small>
                                                        <?php echo $each_one['user_name'] ; ?>
                                                    </a>
                                                    <?php echo $each_one['claim_minute'] ; ?>
                                                </p>
                                            </div>
                                    <?php }} ?>
                                        <!-- /.item -->
                                        <!-- chat item -->

                                        <!-- /.item -->
                                    </div>
                                    <!-- /.chat -->
                                    <div class="box-footer">
                                        <div class="input-group">
                                            <input name="minutes" class="form-control" placeholder="Type message...">
                                            <div class="input-group-btn">
                                                <button name="add_minutes" type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>


<!-- Main Content Ends -->


<?php
// footer section include
include_once('layouts/footer.php');

?>
<script type="text/javascript">
    $(document).ready(function(){

        $("#evidence").change(function(){
            //get the input and UL list
            var input = document.getElementById('evidence');
            var list = document.getElementById('fileList');

            //empty list for now...
            $("#filelist").empty();

            //for every file...
            for (var x = 0; x < input.files.length; x++) {
                var filename = 'File ' + (x + 1) + ':  ' + input.files[x].name;
                var li = "<li>"+filename+"</li>";

                $("#filelist").append(li);
            }
        });



    });
</script>
