<?php
/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */
include_once('common_header.php');

// header section include
include_once('layouts/header.php');
//side Navigation include
include_once('layouts/side_navigation.php');
?>
<?php

// Spenzer starts here

$claim_obj  = new Claim();
$current_user = $_SESSION['user_id'];
if($_SESSION['user_role'] == 1){
    // for student user_role = 1
    $claim_details = $claim_obj->getClaimsForStudent($current_user);
}elseif($_SESSION['user_role'] == 2){
    // for ec_coordinator user_role = 2
    $faculty_obj = new Faculty();
    $faculty_data = $faculty_obj->getFacultyIDByCoID($current_user);
    $claim_details = $claim_obj->getClaimsForFaculty($faculty_data['id']);
}else{
    // for ec_manager and Admin user_role = 3
    $claim_details = $claim_obj->getClaimsAll();
}
$now = time(); // or your date as well




foreach($claim_details as $each_claim){
    if($each_claim['status'] == 0){

        $claim_date = strtotime($each_claim['date']);
        $datediff = $now - $claim_date;
        $gap = floor($datediff / (60 * 60 * 24));
        if($gap > 14){
            $claim_data = array();
            $claim_data['status'] = 2;
            $claim_obj->updateClaim($claim_data,$each_claim['id']);
        }
    }
}

?>




<!-- Main Content Starts -->


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            View Claims
            <small>View Your Claims</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Claims</a></li>
            <li class="active">View Claims</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Your Claims So Far</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Claim Claim Ref Number</th>
                                <th>Claim Title</th>
                                <th>Claim Type</th>
                                <th>Claim Description</th>
                                <?php if($_SESSION['user_role'] == 4){?>
                                <td>
                                    Faculty
                                </td>

                                <?php } ?>
                                <th>Current Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($claim_details)){
                            foreach($claim_details as $each_claim){ ?>
                            <tr align="center">
                                <td>ECSC<?php echo $each_claim['id'] ; ?></td>
                                <td><?php echo $each_claim['claim_title'] ; ?></td>
                                <td><?php echo $each_claim['claim_type'] ; ?></td>
                                <td><?php echo substr($each_claim['claim_description'],0,50)."..." ; ?></td>
                                <?php if($_SESSION['user_role'] == 4){?>
                                    <td>
                                         <?php echo $each_claim['faculty_name']; ?>
                                    </td>

                                <?php } ?>
                                <td><?php
                                    if($each_claim['status'] == 0 ){?>
                                        <small class="label bg-black-gradient">Pending Approval</small>
                                    <?php }  elseif($each_claim['status'] == 1){ ?>
                                        <small class="label  bg-yellow">Notified. Processing Claim</small>
                                    <?php }elseif($each_claim['status'] == 2){ ?>
                                        <small class="label  bg-red-gradient">Expired . Re Enter Claim</small>
                                    <?php } elseif($each_claim['status'] == 3){ ?>
                                        <small class="label  bg-green">Action Taken</small>
                                    <?php }else{ ?>
                                        <small class="label  bg-red-gradient">Rejected</small>
                                    <?php }
                                    ?></td>
                                <td>
                                    <?php if($each_claim['status'] == 2 ){?>
                                    <?php if($_SESSION['user_role'] == 4){?>
                                            <a href="claim_details.php?claim_id=<?php echo $each_claim['id'] ; ?>">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>

                                        <?php } else{ ?>

                                        <?php } ?>
                                    <?php }else{?>

                                        <a href="claim_details.php?claim_id=<?php echo $each_claim['id'] ; ?>">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php }}else{ ?>
                                <tr align="center">
                                    <td colspan="6">No Claims To Display</td>

                                </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Claim Claim Ref Number</th>
                                <th>Claim Title</th>
                                <th>Claim Type</th>
                                <th>Claim Description</th>
                                <?php if($_SESSION['user_role'] == 4){?>
                                    <td>
                                        Faculty
                                    </td>

                                <?php } ?>
                                <th>Current Status</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>


<!-- Main Content Ends -->



<?php
// footer section include

include_once('layouts/footer.php');

?>
<script>
    $(function () {

        $("#example1").DataTable();
    });
</script>