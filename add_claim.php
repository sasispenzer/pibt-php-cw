<?php
/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */
include_once('common_header.php');

// header section include
include_once('layouts/header.php');
//side Navigation include
include_once('layouts/side_navigation.php');


?>


<?php
// Spenzer Starts Here

// Get Claim Types
$claimType_obj = new ClaimType();
$ClaimTypes = $claimType_obj->getAllClaimTypes();

$current_user = $_SESSION['user_id'];
$current_user_program = $_SESSION['program_id'];

// getProgram from DB
$program_obj = new Program();
$program_data = $program_obj->getProgramById($current_user_program);

// getSubjects from DB
$subject_obj = new Subject();
$subjectIDArray = $subject_obj->getSubjectsByPid($current_user_program);
$user_subjects_array = array();
foreach($subjectIDArray as $eachID){
    $sub_details = $subject_obj->getSubjectNameByID($eachID['sub_id']);
    $array = array();
    $array['subject_name'] = $sub_details['subject_name'];
    $array['id'] = $sub_details['id'];
    array_push($user_subjects_array,$array);
}

// after press submit button
if(isset($_POST['submit'])){

    if(!empty($_POST)){
        $claim_data = array();
        $claim_data['claim_title'] =  isset($_POST ['claim_title']) ? $_POST ['claim_title'] : '';
        $claim_data['claim_type_id'] =  isset($_POST ['claim_type']) ? $_POST ['claim_type'] : '';
        $claim_data['claim_description'] =  isset($_POST ['claim_description']) ? $_POST ['claim_description'] : '';
        $claim_data['student_id'] =  $_SESSION['user_id'];
        $claim_data['status'] =  0;
        $claim_data['faculty_id'] =  $program_data['fid'];
        $claim_data['program_id'] =  $program_data['id'];
        $claim_data['date'] =  date("Y-m-d");
        if($_POST ['claim_type'] == 1 || $_POST ['claim_type'] == 5){

            $claim_data['subject_id'] = $_POST['subject'];
        }

        try {
            if(!empty($_FILES['evidence'])){
                if($claim_data['claim_title']=='')throw new Exception("Please Enter a Valid Claim Title");
                if($claim_data['claim_type_id'] =='')throw new Exception("Please Enter a Valid Claim Type");
                if($claim_data['claim_description'] =='')throw new Exception("Please Enter a Valid Claim Description");
                if($claim_data['faculty_id'] =='')throw new Exception("Please Enter a Valid Faculty ID");
                if($claim_data['program_id'] =='')throw new Exception("Please Enter a Valid Program ID");

                $claim_obj = new Claim();
                $claim_id = $claim_obj->addClaim($claim_data);

                if(count($_FILES['evidence'])) {
                    $count = 0;
                    define("UPLOAD_DIR", "uploads/evidence/");
                    foreach ($_FILES['evidence']['name'] as $file) {

                        $name = preg_replace("/[^A-Z0-9._-]/i", "_", $file);
                        $i = 0;
                        $parts = pathinfo($file);
                        $extentionArray = array("jpg","JPG","JPEG","jpeg","png","PNG","PDF","pdf");
                        if (!in_array($parts["extension"], $extentionArray)) {
                            throw new Exception("Your Evidence file must be in PDF/PNG or JPG Formats !");
                        }
                        while(file_exists(UPLOAD_DIR . $name)) {
                            $i++;
                            $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
                        }
                        $data_array = array();
                        $data_array['evidence_image'] = $name;
                        $data_array['claim_id'] = $claim_id;
                        $claim_obj->saveEvidece($data_array);
                        $success = move_uploaded_file($_FILES['evidence']["tmp_name"][$count], UPLOAD_DIR . $name);
                        // set proper permissions on the new file
                        chmod(UPLOAD_DIR . $name, 0644);
                        $count++;
                    }
                }
                $success = "Your Claim added Successfully !" ;

                $claim_code = "ECSC".$claim_id;
                $user_obj = new User();
                $user_data = $user_obj->getUserByID($_SESSION['user_id']);
                $receiver = $user_data['email'];
                $type = 1;
                $email_obj = new Email();

                $email_obj->sendEmail($receiver,$type,$claim_code); // send email to student

                $faculty_obj = new Faculty();
                $cor_data = $faculty_obj->getCoIDByFID($program_data['fid']);
                $user_data = $user_obj->getUserByID($cor_data['co_id']);
                $receiver = $user_data['email'];
                $type = 2;
                $email_obj->sendEmail($receiver,$type,$claim_code); // send email to EC Co


            }else{
                $error_message = "You Must Upload Evidence to add a Claim !";
            }


        } catch (Exception $exc) {
            $error_message = $exc->getMessage();
        }
    }
}




?>


<!-- Main Content Starts -->


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Claim Management
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Claim Management</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add a new Claim</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Please fill all Details to add a Claim</h3>
                            </div>
                            <?php  if(isset($error_message)) { ?>
                            <div class="alert-danger alert">
                                <?php   echo $error_message; ?>
                            </div>
                            <?php } ?>
                            <?php  if(isset($success)) { ?>
                            <div class="alert alert-success">
                                <?php echo $success; ?>
                            </div>
                            <?php } ?>

                            <!-- /.box-header -->
                            <!-- form start -->
                            <form enctype="multipart/form-data" method="post" role="form">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="title">Claim Title</label>
                                        <input type="text" class="form-control" id="claim_title" placeholder="Enter Claim Title" name="claim_title">
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Claim Type</label>
                                        <select class="form-control" name="claim_type" id="claim_type">
                                            <option value="0">Please select a claim Type</option>
                                            <?php
                                             foreach($ClaimTypes as $eachType){?>
                                                 <option id="<?php echo $eachType['id'] ; ?>" value="<?php echo $eachType['id'] ; ?>"><?php echo $eachType['claim_type'] ; ?></option>
                                             <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div id="fname" style="display: none" class="form-group">
                                        <label for="title">Faculty Name</label>
                                        <input value="<?php echo $program_data['faculty_name'] ; ?>" type="text" readonly class="form-control" id="f_id" name="f_id">
                                    </div>
                                    <div id="pname" style="display: none" class="form-group">
                                        <label for="title">Program Name</label>
                                        <input value="<?php echo $program_data['program_name'] ; ?>" type="text" readonly class="form-control" id="p_id" name="p_id">
                                    </div>
                                    <div id="sname" style="display: none"  class="form-group">
                                        <label for="subjecta">Select a Subject</label>
                                        <select class="form-control" name="subject" id="subject">
                                            <?php foreach($user_subjects_array as $each_sub){ ?>
                                                    <option value="<?php echo $each_sub['id'] ; ?>" id="<?php echo $each_sub['id'] ; ?>"><?php echo $each_sub['subject_name'] ; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Claim Description</label>
                                        <textarea    id="claim_description" name="claim_description" class="form-control" rows="3" placeholder="Enter Your Description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="evidence">Evidence Upload</label>
                                        <input type="file" name="evidence[]"  multiple="" id="evidence">

                                        <p class="help-block">File Format - PDF / JPG / PNG.</p>
                                        <ul id="filelist">

                                        </ul>
                                    </div>

                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer">
                                    <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>

                        <!-- /.box -->

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>


<!-- Main Content Ends -->



<?php
// footer section include

include_once('layouts/footer.php');

?>
<script type="text/javascript">
    $(document).ready(function(){

        $("#evidence").change(function(){
            //get the input and UL list
            var input = document.getElementById('evidence');
            var list = document.getElementById('fileList');

            //empty list for now...
            $("#filelist").empty();

            //for every file...
            for (var x = 0; x < input.files.length; x++) {
                var filename = 'File ' + (x + 1) + ':  ' + input.files[x].name;
                var li = "<li>"+filename+"</li>";

                $("#filelist").append(li);
            }
        });

        $("#claim_type").change(function(){

            var claim_type = $('#claim_type').val();
            if(claim_type == '1' || claim_type == '5'){
                $('#fname').show('slow');
                $('#pname').show('slow');
                $('#sname').show('slow');
            }else{
                $('#fname').hide('slow');
                $('#pname').hide('slow');
                $('#sname').hide('slow');
            }
        });

    });
</script>