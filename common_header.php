<?php
session_start();


function __autoload($class_name) {
    require_once 'class/'.$class_name . '.php';

}

if(!isset($_SESSION['user_id'])){

    header('Location: login.php');
}

?>