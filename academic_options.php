<?php
/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */
include_once('common_header.php');
if($_SESSION['user_role'] != 4){
    header("Location:index.php");
}
// header section include
include_once('layouts/header.php');
//side Navigation include
include_once('layouts/side_navigation.php');



?>


<?php
// Spenzer Starts Here
$program_obj = new Program();
$programs = $program_obj->getPrograms();

$sub_obj = new Subject();
$subjects = $sub_obj->getSubjects();


if(isset($_POST['submit'])) {
    $data = array();
    $data['pro_id'] = isset($_POST ['pro_id']) ? $_POST ['pro_id'] : '';
    $data['sub_id'] = isset($_POST ['sub_id']) ? $_POST ['sub_id'] : '';
    try {

        if($data['pro_id']=='')throw new Exception("Please Enter a Valid Program");
        if($data['sub_id'] =='')throw new Exception("Please Enter a Valid Subject");

        if($sub_obj->checkSubjectProgram($data['pro_id'],$data['sub_id'])){
            throw new Exception("Subject is already added to that Program !");
        }else{
            $sub_obj->addSubjectToProgram($data);
            $success = "Subject Successfully added to the Program ! ";
        }


    }catch (Exception $exc) {
        $error_message = $exc->getMessage();
    }
}

?>


<!-- Main Content Starts -->


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            University  Management
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add Subjects to Program</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Please Add Subjects to Programs</h3>
                            </div>
                            <?php  if(isset($error_message)) { ?>
                                <div class="alert-danger alert">
                                    <?php   echo $error_message; ?>
                                </div>
                            <?php } ?>
                            <?php  if(isset($success)) { ?>
                                <div class="alert alert-success">
                                    <?php echo $success; ?>
                                </div>
                            <?php } ?>

                            <!-- /.box-header -->
                            <!-- form start -->
                            <form enctype="multipart/form-data" method="post" role="form">
                                <div class="box-body">

                                    <div class="form-group">
                                        <label for="type">Program</label>
                                        <select class="form-control" name="pro_id" id="pro_id">

                                            <?php
                                            foreach($programs as $each_program){?>
                                                <option id="<?php echo $each_program['id'] ; ?>" value="<?php echo $each_program['id'] ; ?>"><?php echo $each_program['program_name'] ; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Subject</label>
                                        <select class="form-control" name="sub_id" id="sub_id">

                                            <?php
                                            foreach($subjects as $each_sub){?>
                                                <option id="<?php echo $each_sub['id'] ; ?>" value="<?php echo $each_sub['id'] ; ?>"><?php echo $each_sub['subject_name'] ; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>


                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer">
                                    <button name="submit" type="submit" class="btn btn-primary">Add Subject </button>
                                </div>
                            </form>
                        </div>

                        <!-- /.box -->

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>


<!-- Main Content Ends -->



<?php
// footer section include

include_once('layouts/footer.php');

?>
