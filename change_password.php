<?php
/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */
include_once('common_header.php');

// header section include
include_once('layouts/header.php');
//side Navigation include
include_once('layouts/side_navigation.php');
?>


<?php
// Spenzer Starts Here
$user_obj = new User();
$coordinators = $user_obj->getCoUser();


if(isset($_POST['submit'])){
    $data = array();
    $data['password'] =  isset($_POST ['password']) ? $_POST ['password'] : '';
    $password_2 =  isset($_POST ['password_2']) ? $_POST ['password_2'] : '';

    try {
        if (!empty($data)) {
            if ($data['password'] == '') throw new Exception("Please Enter a Valid Password");
            if ($data['password'] != $password_2) throw new Exception("Passwords are not Matched !");
            $data['password'] = md5($data['password']);
            $user_obj->change_password($data,$_SESSION['user_id']);
            $success = "Password Updated Successfully ! ";
        }
    }
    catch (Exception $exc) {
        $error_message = $exc->getMessage();
    }
}

?>


<!-- Main Content Starts -->


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Profile  Management
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Profile Management</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Change Your Password</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Please Enter Twice your new Password</h3>
                            </div>
                            <?php  if(isset($error_message)) { ?>
                                <div class="alert-danger alert">
                                    <?php   echo $error_message; ?>
                                </div>
                            <?php } ?>
                            <?php  if(isset($success)) { ?>
                                <div class="alert alert-success">
                                    <?php echo $success; ?>
                                </div>
                            <?php } ?>

                            <!-- /.box-header -->
                            <!-- form start -->
                            <form enctype="multipart/form-data" method="post" role="form">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="title">New Password</label>
                                        <input type="text" class="form-control" id="password" placeholder="Enter  Password" name="password">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Re Enter New Password</label>
                                        <input type="text" class="form-control" id="password_2" placeholder="Re Enter Password" name="password_2">
                                    </div>



                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer">
                                    <button name="submit" type="submit" class="btn btn-primary">Change Password</button>
                                </div>
                            </form>
                        </div>

                        <!-- /.box -->

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>


<!-- Main Content Ends -->



<?php
// footer section include

include_once('layouts/footer.php');

?>
