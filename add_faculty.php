<?php
/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */
include_once('common_header.php');
if($_SESSION['user_role'] != 4){
    header("Location:index.php");
}
// header section include
include_once('layouts/header.php');
//side Navigation include
include_once('layouts/side_navigation.php');



?>


<?php
// Spenzer Starts Here
$user_obj = new User();
$coordinators = $user_obj->getCoUser();


if(isset($_POST['submit'])) {
    $faculty_data = array();
    $faculty_data['faculty_name'] = isset($_POST ['faculty_name']) ? $_POST ['faculty_name'] : '';
    $faculty_data['co_id'] = isset($_POST ['user_id']) ? $_POST ['user_id'] : '';
    try {
        if (!empty($faculty_data)) {
            if ($faculty_data['faculty_name'] == '') throw new Exception("Please Enter a Valid Faculty Name");
            if ($faculty_data['co_id'] == '') throw new Exception("Please Enter a Valid Coordinator Name");
            $faculty_obj = new Faculty();
            $faculty_obj->addFaculty($faculty_data);
            $success = "New Faculty Added Successfully ! ";

        }
    }catch (Exception $exc) {
        $error_message = $exc->getMessage();
    }
}

?>


<!-- Main Content Starts -->


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            University  Management
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add a new Faculty</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Please fill all Details to add a Faculty</h3>
                            </div>
                            <?php  if(isset($error_message)) { ?>
                                <div class="alert-danger alert">
                                    <?php   echo $error_message; ?>
                                </div>
                            <?php } ?>
                            <?php  if(isset($success)) { ?>
                                <div class="alert alert-success">
                                    <?php echo $success; ?>
                                </div>
                            <?php } ?>

                            <!-- /.box-header -->
                            <!-- form start -->
                            <form enctype="multipart/form-data" method="post" role="form">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="title">Faculty Name</label>
                                        <input type="text" class="form-control" id="faculty_name" placeholder="Enter Faculty Name" name="faculty_name">
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Coordinator</label>
                                        <select class="form-control" name="user_id" id="user_id">

                                            <?php
                                            foreach($coordinators as $each_user){?>
                                                <option id="<?php echo $each_user['id'] ; ?>" value="<?php echo $each_user['id'] ; ?>"><?php echo $each_user['user_name'] ; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>


                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer">
                                    <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>

                        <!-- /.box -->

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>


<!-- Main Content Ends -->



<?php
// footer section include

include_once('layouts/footer.php');

?>
