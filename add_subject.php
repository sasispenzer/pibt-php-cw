<?php
/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */
include_once('common_header.php');
if($_SESSION['user_role'] != 4){
    header("Location:index.php");
}
// header section include
include_once('layouts/header.php');
//side Navigation include
include_once('layouts/side_navigation.php');


?>


<?php
// Spenzer Starts Here
$user_obj = new User();
$coordinators = $user_obj->getCoUser();


if(isset($_POST['submit'])){
    $Subject_data = array();
    $Subject_data['subject_name'] =  isset($_POST ['subject_name']) ? $_POST ['subject_name'] : '';

    try {
        if (!empty($Subject_data)) {
            if ($Subject_data['subject_name'] == '') throw new Exception("Please Enter a Valid Subject Name");
            $subject_obj = new Subject();
            $subject_obj->addSubject($Subject_data);
            $success = "New Subject Added Successfully ! ";
        }
    }
    catch (Exception $exc) {
        $error_message = $exc->getMessage();
    }
}

?>


<!-- Main Content Starts -->


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            University  Management
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add a new Subject</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Please fill all Details to add a Subject</h3>
                            </div>
                            <?php  if(isset($error_message)) { ?>
                                <div class="alert-danger alert">
                                    <?php   echo $error_message; ?>
                                </div>
                            <?php } ?>
                            <?php  if(isset($success)) { ?>
                                <div class="alert alert-success">
                                    <?php echo $success; ?>
                                </div>
                            <?php } ?>

                            <!-- /.box-header -->
                            <!-- form start -->
                            <form enctype="multipart/form-data" method="post" role="form">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="title">Subject Name</label>
                                        <input type="text" class="form-control" id="subject_name" placeholder="Enter Subject Name" name="subject_name">
                                    </div>



                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer">
                                    <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>

                        <!-- /.box -->

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>


<!-- Main Content Ends -->



<?php
// footer section include

include_once('layouts/footer.php');

?>
