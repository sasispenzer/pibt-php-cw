<?php
/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */

?>
<!-- =============================================== -->

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="dist/img/54-128.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $_SESSION['user_name'] ; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="index.php">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">

            </span>
                </a>
            </li>
            <?php if($_SESSION['user_role'] == 4){?>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-university"></i>
                    <span>University Management</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="add_faculty.php"><i class="fa fa-circle-o"></i> Add a New Faculty</a></li>
                    <li><a href="add_subject.php"><i class="fa fa-circle-o"></i> Add a New Subject</a></li>
                    <li><a href="add_program.php"><i class="fa fa-circle-o"></i> Add a New Program</a></li>
                    <li><a href="add_user.php"><i class="fa fa-circle-o"></i> Add a New User</a></li>
                    <li><a href="academic_options.php"><i class="fa fa-circle-o"></i> Academic Management</a></li>
                    <li><a href="view_academic.php"><i class="fa fa-circle-o"></i> View Academic Details</a></li>

                </ul>
            </li>
            <?php } ?>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Claims Management</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <?php if($_SESSION['user_role'] == 1){?>
                    <li><a href="add_claim.php"><i class="fa fa-circle-o"></i> Add a New Claim</a></li>
                    <?php } ?>
                    <li><a href="view_claims.php"><i class="fa fa-circle-o"></i> View My Claims</a></li>


                </ul>
            </li>


        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->
