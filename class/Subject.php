<?php
/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 * Email : Sasi.spenzer@gmail.com
 * Telephone :  +94 71 42 300 57
 */
class Subject
{

    public function getSubjectNameByID($id){
        $db_obj = new DB();
        $sql = "SELECT *
                                FROM
                                    subjects
                                WHERE
                                    id = '".$id."'";
        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $subject_details_by_id = $db_obj->getRow($sql);
            $extra_obj = new Extra();
            $subject_details = $extra_obj->objectToArray($subject_details_by_id);
            return $subject_details;
        } else {
            return false;
        }
    }
    public function checkSubjectProgram($pid,$sid){
        $db_obj = new DB();
        $sql = "SELECT *
                                FROM
                                    program_subject_details
                                WHERE
                                    pro_id = '".$pid."' AND sub_id = '".$sid."' ";
        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getSubjectsByPid($pid){
        $db_obj = new DB();
        $sql = "SELECT *
                                FROM
                                    program_subject_details
                                WHERE
                                    pro_id = '".$pid."'";
        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $subject_details_by_id = $db_obj->getResults($sql);
            $extra_obj = new Extra();
            $subject_details = $extra_obj->objectToArray($subject_details_by_id);
            return $subject_details;
        } else {
            return false;
        }
    }

    public function getSubjects(){
        $db_obj = new DB();
        $sql = "SELECT *
                                FROM
                                    subjects";

        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $subject_details_by_id = $db_obj->getResults($sql);
            $extra_obj = new Extra();
            $subject_details = $extra_obj->objectToArray($subject_details_by_id);
            return $subject_details;
        } else {
            return false;
        }
    }

    public function addSubject($data){
        $db_obj = new DB();
        $table ='subjects';
        $results = $db_obj->insert($table,$data);
        return $results;
    }
    public function addSubjectToProgram($data){
        $db_obj = new DB();
        $table ='program_subject_details';
        $results = $db_obj->insert($table,$data);
        return $results;
    }

}