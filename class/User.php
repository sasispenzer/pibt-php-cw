<?php

/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 * Email : Sasi.spenzer@gmail.com
 * Telephone :  +94 71 42 300 57
 */
class User

{

    public function getUserByID($id) {

        $db_obj = new DB();
        $user_query = "SELECT *
                                FROM
                                    users
                                WHERE
                                    id = '".$id."'";
        $db_obj->query($user_query);
        if($db_obj->rowCount() > 0) {
            $user_details_by_id = $db_obj->getRow($user_query);
            $extra_obj = new Extra();
            $user_details = $extra_obj->objectToArray($user_details_by_id);
            return $user_details;
        } else {
            return false;
        }

    }

    public function change_password($data,$id){
        $db_obj = new DB();
        $table = 'users';
        $data = $data;

        $where = "id =".$id;
        $db_obj->update($table, $data, $where);
    }

    public function login_user($user_details) {

        $email =  $user_details['email'];
        $password  =  md5($user_details['password']);

        $db_obj = new DB();
        $user_details_sql = "SELECT
                                    id,
                                    user_name,
                                    password,
                                    user_role,
                                    user_status,
                                    program_id
                                FROM
                                    users
                                WHERE
                                    email = '".$email."'
                                AND
                                    password = '".$password."'";

        $db_obj->query($user_details_sql);

        if($db_obj->rowCount() > 0) {

            $user_details = $db_obj->getRow($user_details_sql);
            $extra_obj = new Extra();
            $logged_user_details = $extra_obj->objectToArray($user_details);
            return $logged_user_details;

        } else {
            return false;
        }


    }

    public function update_last_login($user_id) {
        $db_obj = new DB();
        $update_last_login_sql = "UPDATE
                                users
                                SET
                                 last_login = NOW()
                                WHERE
                                    id = ".$user_id ;

        $db_obj->query($update_last_login_sql);
    }

    public function insert_user_log($user_data) {
        $db_obj = new DB();
        $table = 'user_login_history';
        $db_obj->insert($table, $user_data);

    }

    public function get_user_availability($email) {
        $db_obj = new DB();
        $sql = "SELECT  username
                FROM
                        users
                WHERE
                        email='$email'";
        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            return 'true';
        }

    }


    public function getCoUser(){
        $db_obj = new DB();
        $user_query = "SELECT *
                                FROM
                                    users
                                WHERE
                                    user_role = '2'";
        $db_obj->query($user_query);
        if($db_obj->rowCount() > 0) {
            $user_details_by_id = $db_obj->getResults($user_query);
            $extra_obj = new Extra();
            $user_details = $extra_obj->objectToArray($user_details_by_id);
            return $user_details;
        } else {
            return false;
        }

    }


    public function add_new_user($data){
        $db_obj = new DB();
        $table ='users';
        $results = $db_obj->insert($table,$data);
        return $results;
    }

}

