<?php

/**
 * Created by PhpStorm.
 * User: galle
 * Date: 03-Apr-17
 * Time: 15:10
 */
class UserRole
{

    public function getUserRoles(){
        $db_obj = new DB();
        $sql = "SELECT
                                    *
                                FROM
                                    user_roles";
        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $roles_details = $db_obj->getResults($sql);
            $extra_obj = new Extra();
            $user_roles_data = $extra_obj->objectToArray($roles_details);
            return $user_roles_data;
        } else {
            return false;
        }
    }

}