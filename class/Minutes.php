<?php

/**
 * Created by PhpStorm.
 * User: galle
 * Date: 05-Apr-17
 * Time: 09:41
 */
class Minutes
{

    public function getMinutesByClaimID($id){
        $db_obj = new DB();
        $faculty_qry = "SELECT
                                    claim_minutes.*,users.user_name
                                FROM
                                    claim_minutes INNER JOIN
                                    users ON
                                    users.id = claim_minutes.user_id
                                WHERE claim_minutes.claim_id = '".$id."'";


        $db_obj->query($faculty_qry);
        if($db_obj->rowCount() > 0) {
            $faculty_details = $db_obj->getResults($faculty_qry);
            $extra_obj = new Extra();
            $faculty_data = $extra_obj->objectToArray($faculty_details);
            return $faculty_data;
        } else {
            return false;
        }
    }

    public function add_minute($data){
        $db_obj = new DB();
        $table ='claim_minutes';
        $results = $db_obj->insert($table,$data);
        return $results;
    }


}