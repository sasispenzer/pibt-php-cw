<?php

/**
 * Created by PhpStorm.
 * User: galle
 * Date: 04-Apr-17
 * Time: 14:46
 */
class Email
{

    public function sendEmail($receiver,$type,$code){
        $mail = new PHPMailer;

        $mail->isSMTP();                                   // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';                    // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                            // Enable SMTP authentication
        $mail->Username = 'galleteashop@gmail.com';          // SMTP username
        $mail->Password = 'richard@123'; // SMTP password
        $mail->SMTPSecure = 'tls';                         // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                 // TCP port to connect to

        $mail->setFrom('galleteashop@gmail.com', 'Galle Tea shop');
        $mail->addReplyTo('galleteashop@gmail.com', 'Galle Tea Shop');
        $mail->addAddress($receiver);   // Add a recipient
        //$mail->addAddress('sasi.spenzer@gmail.com');   // Add a recipient
        //$mail->addCC('cc@example.com');
        $mail->addBCC('sasi.spenzer@gmail.com');

        $mail->isHTML(true);  // Set email format to HTML
        if($type == 1){
            $bodyContent = '<h3>Thank you for Adding a Claim</h3>';
            $bodyContent .= '<p>This is an auto-genarated email by the EC Management System</p>';
            $bodyContent .= '<p>You can easily track your Claim using following Claim ID</p>';
            $bodyContent .= '<h2>Your Claim ID : '.$code.'</h2>';
        }elseif($type == 2){
            $bodyContent = '<h3>You Have a New Claim Request</h3>';
            $bodyContent .= '<p>This is an auto-genarated email by the EC Management System</p>';
            $bodyContent .= '<p>You can easily track your Claim using following Claim ID</p>';
            $bodyContent .= '<h2>Your Claim ID : '.$code.'</h2>';
        } elseif($type == 3){
            $bodyContent = '<h3>Your'.$code.' Claim was updated</h3>';
            $bodyContent .= '<p>This is an auto-genarated email by the EC Management System</p>';
            $bodyContent .= '<p>You can easily track your Claim using following Claim ID</p>';
            $bodyContent .= '<h2>Your Claim ID : '.$code.'</h2>';
        }
            $bodyContent .= '<p align="center">This is an auto-genarated email by the EC Management System. Developed By <b>Sasi Spenzer 2017</b> &copy;</p>';




        $mail->Subject = 'Email from EC Management System';
        $mail->Body    = $bodyContent;

        if(!$mail->send()) {
           return false;
        } else {
            return true;
        }
    }


}