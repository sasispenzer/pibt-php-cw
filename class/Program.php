<?php

/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 * Email : Sasi.spenzer@gmail.com
 * Telephone :  +94 71 42 300 57
 */
class Program
{

    public function getProgramById($pid){
        $db_obj = new DB();
        $sql = "SELECT  programs.*,faculties.id AS fid,faculties.faculty_name AS faculty_name
                FROM programs
                INNER JOIN faculties
                ON faculties.id = programs.faculty_id
                WHERE programs.id='$pid'";

        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $program_details_by_id = $db_obj->getRow($sql);
            $extra_obj = new Extra();
            $program_details = $extra_obj->objectToArray($program_details_by_id);
            return $program_details;
        }else{
            return false;
        }
    }

    public function getProgramstoSubject(){
        $db_obj = new DB();
        $sql = "SELECT program_subject_details.*,subjects.subject_name,programs.program_name
                  FROM program_subject_details
                  INNER JOIN programs ON programs.id = program_subject_details.pro_id
                  INNER JOIN subjects ON subjects.id = program_subject_details.sub_id";

        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $subject_details_by_id = $db_obj->getResults($sql);
            $extra_obj = new Extra();
            $subject_details = $extra_obj->objectToArray($subject_details_by_id);
            return $subject_details;
        } else {
            return false;
        }
    }



    public function getPrograms(){
        $db_obj = new DB();
        $sql = "SELECT  programs.*,faculties.id AS fid,faculties.faculty_name AS faculty_name
                FROM programs
                INNER JOIN faculties
                ON faculties.id = programs.faculty_id ";


        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $program_details_by_id = $db_obj->getResults($sql);
            $extra_obj = new Extra();
            $program_details = $extra_obj->objectToArray($program_details_by_id);
            return $program_details;
        }else{
            return false;
        }
    }

    public function addProgram($data){
        $db_obj = new DB();
        $table ='programs';
        $results = $db_obj->insert($table,$data);
        return $results;
    }





}