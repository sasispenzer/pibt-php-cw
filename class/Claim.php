<?php

/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */
class Claim
{

    public function addClaim($data){
        $db_obj = new DB();
        $table = 'claims';
        $db_obj->insert($table, $data);
        $id = $db_obj->getLastInsertId();
        return $id ;
    }
    public function saveEvidece($data){
        $db_obj = new DB();
        $table = 'claim_evidence';
        $db_obj->insert($table, $data);
    }

    public function getClaimsForStudent($student_id){
        $db_obj = new DB();
        $sql = "SELECT claims.*,claim_type.claim_type AS claim_type
                                FROM
                                    claims
                                INNER JOIN claim_type
                                ON claim_type.id = claims.claim_type_id
                                WHERE
                                    claims.student_id	 = '".$student_id."'";
        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $claim_details_by_id = $db_obj->getResults($sql);
            $extra_obj = new Extra();
            $claim_details = $extra_obj->objectToArray($claim_details_by_id);
            return $claim_details;
        } else {
            return false;
        }
    }
    public function getClaimsAll(){
        $db_obj = new DB();
        $sql = "SELECT claims.*,claim_type.claim_type AS claim_type,faculties.faculty_name AS faculty_name
                                FROM
                                    claims
                                INNER JOIN claim_type
                                ON claim_type.id = claims.claim_type_id
                                INNER JOIN faculties
                                ON faculties.id = claims.faculty_id
                                ";

        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $claim_details_by_id = $db_obj->getResults($sql);
            $extra_obj = new Extra();
            $claim_details = $extra_obj->objectToArray($claim_details_by_id);
            return $claim_details;
        } else {
            return false;
        }
    }

    public function getEvidence($id){
        $db_obj = new DB();
        $sql = "SELECT *
                                FROM
                                    claim_evidence
                                WHERE
                                    claim_id	 = '".$id."'";
        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $claim_details_by_id = $db_obj->getResults($sql);
            $extra_obj = new Extra();
            $claim_details = $extra_obj->objectToArray($claim_details_by_id);
            return $claim_details;
        } else {
            return false;
        }
    }


    public function getClaimsForFaculty($faculty_id){
        $db_obj = new DB();
        $sql = "SELECT claims.*,claim_type.claim_type AS claim_type
                                FROM
                                    claims
                                INNER JOIN claim_type
                                ON claim_type.id = claims.claim_type_id
                                WHERE
                                    claims.faculty_id	 = '".$faculty_id."'";
        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $claim_details_by_id = $db_obj->getResults($sql);
            $extra_obj = new Extra();
            $claim_details = $extra_obj->objectToArray($claim_details_by_id);
            return $claim_details;
        } else {
            return false;
        }
    }
    public function getClaimsByID($cid){
        $db_obj = new DB();
        $sql = "SELECT claims.*,claim_type.claim_type AS claim_type,claim_type.id AS claim_id,faculties.faculty_name AS faculty_name
                                FROM
                                    claims
                                INNER JOIN claim_type
                                ON claim_type.id = claims.claim_type_id
                                INNER JOIN faculties
                                ON faculties.id = claims.faculty_id
                                WHERE
                                    claims.id	 = '".$cid."'";
        $db_obj->query($sql);
        if($db_obj->rowCount() > 0) {
            $claim_details_by_id = $db_obj->getRow($sql);
            $extra_obj = new Extra();
            $claim_details = $extra_obj->objectToArray($claim_details_by_id);
            return $claim_details;
        } else {
            return false;
        }
    }

    public function updateClaim($claimData,$id){

        $db_obj = new DB();
        $table = 'claims';
        $data = $claimData;

        $where = "id =".$id;
        $db_obj->update($table, $data, $where);


    }

}