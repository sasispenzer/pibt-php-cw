<?php

/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 * Email : Sasi.spenzer@gmail.com
 * Telephone :  +94 71 42 300 57
 */
class ClaimType
{

    public function getAllClaimTypes(){
        $db_obj = new DB();
        $type_details = "SELECT
                                    *
                                FROM
                                    claim_type
                                WHERE
                                    status = '1'";

        $db_obj->query($type_details);
        if($db_obj->rowCount() > 0) {
            $Ctype_details = $db_obj->getResults($type_details);
            $extra_obj = new Extra();
            $claim_type_data = $extra_obj->objectToArray($Ctype_details);
            return $claim_type_data;
        } else {
            return false;
        }
    }

}