<?php

/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */


class Faculty

{

    public function getFacultyIDByCoID($coID){

        $db_obj = new DB();
        $faculty_qry = "SELECT
                                    *
                                FROM
                                    faculties
                                WHERE
                                    co_id = '".$coID."'";

        $db_obj->query($faculty_qry);
        if($db_obj->rowCount() > 0) {
            $faculty_details = $db_obj->getRow($faculty_qry);
            $extra_obj = new Extra();
            $faculty_data = $extra_obj->objectToArray($faculty_details);
            return $faculty_data;
        } else {
            return false;
        }
    }
    public function getCoIDByFID($fid){

        $db_obj = new DB();
        $faculty_qry = "SELECT
                                    *
                                FROM
                                    faculties
                                WHERE id = '".$fid."'";


        $db_obj->query($faculty_qry);
        if($db_obj->rowCount() > 0) {
            $faculty_details = $db_obj->getRow($faculty_qry);
            $extra_obj = new Extra();
            $faculty_data = $extra_obj->objectToArray($faculty_details);
            return $faculty_data;
        } else {
            return false;
        }
    }
    public function getFacultyAll(){

        $db_obj = new DB();
        $faculty_qry = "SELECT
                                    *
                                FROM
                                    faculties    ";


        $db_obj->query($faculty_qry);
        if($db_obj->rowCount() > 0) {
            $faculty_details = $db_obj->getResults($faculty_qry);
            $extra_obj = new Extra();
            $faculty_data = $extra_obj->objectToArray($faculty_details);
            return $faculty_data;
        } else {
            return false;
        }
    }

    public function addFaculty($data){
        $db_obj = new DB();
        $table ='faculties';
        $results = $db_obj->insert($table,$data);
        return $results;
    }


}