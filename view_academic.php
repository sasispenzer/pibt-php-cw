<?php
/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */
include_once('common_header.php');
if($_SESSION['user_role'] != 4){
    header("Location:index.php");
}
// header section include
include_once('layouts/header.php');
//side Navigation include
include_once('layouts/side_navigation.php');



?>
<?php

// Spenzer starts here

$program_obj = new Program();
$total_data = $program_obj->getProgramstoSubject();

$programSubjectArray = array();
$count = 0;
foreach($total_data as $each_data){

    if(!isset($programSubjectArray[$each_data['pro_id']])){
        $programSubjectArray[$each_data['pro_id']] = array();

    }

    $programSubjectArray[$each_data['pro_id']]['program'] = $each_data['program_name'];
    if(!isset($programSubjectArray[$each_data['pro_id']]['subjects'])){
        $programSubjectArray[$each_data['pro_id']]['subjects'] = array();
    }
    array_push($programSubjectArray[$each_data['pro_id']]['subjects'],$each_data['subject_name']);


}

?>




<!-- Main Content Starts -->


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            View Claims
            <small>View Academic</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Claims</a></li>
            <li class="active">View Academic</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Programs and Subjects</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Program id</th>
                                <th>Program</th>
                                <th>Subjects</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($programSubjectArray)){
                            foreach($programSubjectArray as $key=> $each_pro){ ?>
                            <tr align="center">
                                <td><?php echo $key ; ?></td>
                                <td><?php echo $each_pro['program'] ; ?></td>
                                <td>
                                    <?php foreach($each_pro['subjects'] as $each_sub) {
                                        echo $each_sub,"  ,   ";
                                     }?>

                                </td>

                            </tr>
                            <?php }}else{ ?>
                                <tr align="center">
                                    <td colspan="3">No Programs To Display</td>

                                </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Program id</th>
                                <th>Program</th>
                                <th>Subjects</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>


<!-- Main Content Ends -->



<?php
// footer section include

include_once('layouts/footer.php');

?>
<script>
    $(function () {

        $("#example1").DataTable();
    });
</script>