<?php
/**
 * Created by Sasi Spenzer.
 * User: Spenzer PC
 * Date: 18-Mar-17
 * Time: 10:00
 */
include_once('common_header.php');
if($_SESSION['user_role'] != 4){
    header("Location:index.php");
}
// header section include
include_once('layouts/header.php');
//side Navigation include
include_once('layouts/side_navigation.php');



?>


<?php


// Spenzer Starts Here
$program_obj = new Program();
$programs = $program_obj->getPrograms();

$user_role_obj = new UserRole();
$user_roles = $user_role_obj->getUserRoles();


if(isset($_POST['submit'])) {
    $user_data = array();
    $user_data['user_name'] = isset($_POST ['user_name']) ? $_POST ['user_name'] : '';
    $user_data['email'] = isset($_POST ['email']) ? $_POST ['email'] : '';
    $user_data['user_role'] = isset($_POST ['user_role']) ? $_POST ['user_role'] : '';
    $user_data['DOB'] = isset($_POST ['DOB']) ? $_POST ['DOB'] : '';
    $user_data['contact_number'] = isset($_POST ['contact_number']) ? $_POST ['contact_number'] : '';
    $user_data['Address'] = isset($_POST ['Address']) ? $_POST ['Address'] : '';
    $user_data['program_id'] = isset($_POST ['program_id']) ? $_POST ['program_id'] : '';
    $user_data['user_status'] = 1;
    $user_data['password'] =  md5($user_data['email']."@123");

    try {
        if (!empty($user_data)) {
            if($user_data['user_name']=='')throw new Exception("Please Enter a Valid User Name");
            if($user_data['email'] =='')throw new Exception("Please Enter a Valid Email");
            if($user_data['DOB'] =='')throw new Exception("Please Enter a Valid Date");
            if($user_data['contact_number'] =='')throw new Exception("Please Enter a Valid Contact Number");
            if($user_data['program_id'] =='')throw new Exception("Please Enter a Valid Program ID");

            $user_obj = new User();
            $user_obj->add_new_user($user_data);
            $success = "New User Added Successfully ! ";
        }
    }catch (Exception $exc) {
        $error_message = $exc->getMessage();
    }
}

?>


<!-- Main Content Starts -->


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            University  Management
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add a new User</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Please fill all Details to add a User</h3>
                            </div>
                            <?php  if(isset($error_message)) { ?>
                                <div class="alert-danger alert">
                                    <?php   echo $error_message; ?>
                                </div>
                            <?php } ?>
                            <?php  if(isset($success)) { ?>
                                <div class="alert alert-success">
                                    <?php echo $success; ?>
                                </div>
                            <?php } ?>

                            <!-- /.box-header -->
                            <!-- form start -->
                            <form enctype="multipart/form-data" method="post" role="form">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="title">User Name</label>
                                        <input type="text" class="form-control" id="user_name" placeholder="Enter User Name" name="user_name">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Email</label>
                                        <input type="email" class="form-control" id="email" placeholder="Enter User Email" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Select User Role</label>
                                        <select class="form-control" name="user_role" id="user_role">

                                            <?php
                                            foreach($user_roles as $each_role){?>
                                                <option id="<?php echo $each_role['id'] ; ?>" value="<?php echo $each_role['id'] ; ?>"><?php echo $each_role['user_role'] ; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Date of Birth</label>
                                        <input type="text" class="form-control" id="datepicker" placeholder="Enter User Date of Birth" name="DOB">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Contact Number</label>
                                        <input type="number" class="form-control" id="contact_number" placeholder="Enter User Contact Number" name="contact_number">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Address</label>
                                        <textarea class="form-control" id="Address" name="Address"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Select Program</label>
                                        <select class="form-control" name="program_id" id="program_id">

                                            <?php
                                            foreach($programs as $each_program){?>
                                                <option id="<?php echo $each_program['id'] ; ?>" value="<?php echo $each_program['id'] ; ?>"><?php echo $each_program['program_name'] ; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer">
                                    <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>

                        <!-- /.box -->

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>


<!-- Main Content Ends -->



<?php
// footer section include

include_once('layouts/footer.php');

?>
<script type="text/javascript">
    $('#datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd"
    });
</script>